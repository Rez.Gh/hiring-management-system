package controller;

public class GeneralController {
    private static GeneralController generalController;
    private ApplyController applyController;
    private CompanyController companyController;
    private ContractTypeController contractTypeController;
    private EmployeeController employeeController;
    private JobController jobController;
    private JobPositionController jobPositionController;

    private GeneralController() {
        applyController = new ApplyController();
        companyController = new CompanyController();
        contractTypeController = new ContractTypeController();
        employeeController = new EmployeeController();
        jobController = new JobController();
        jobPositionController = new JobPositionController();
    }

    public static GeneralController getInstance() {
        if (generalController == null)
            generalController = new GeneralController();
        return generalController;
    }

    public ApplyController getApplyController() {
        return applyController;
    }

    public CompanyController getCompanyController() {
        return companyController;
    }

    public ContractTypeController getContractTypeController() {
        return contractTypeController;
    }

    public EmployeeController getEmployeeController() {
        return employeeController;
    }

    public JobController getJobController() {
        return jobController;
    }

    public JobPositionController getJobPositionController() {
        return jobPositionController;
    }
}
