package controller;

import model.Employee;
import model.JobPosition;
import service.EmployeeService;
import service.GeneralService;
import utility.ObjectNotFoundException;

import java.util.List;

public class EmployeeController implements Controller<Employee> {
    public static Employee currentEmployee;
    private EmployeeService employeeService = GeneralService.getInstance().getEmployeeService();

    public void insert(Employee employee) {
        employeeService.insert(employee);
    }

    public void update(Employee employee) {
        employeeService.update(employee);
    }

    public Employee searchById(Long id) throws ObjectNotFoundException {
        return employeeService.searchById(id);
    }

    public List<Employee> findAll() {
        return employeeService.findAll();
    }

    public Employee searchByUsername(String username){
        return employeeService.searchByUsername(username);
    }

    public List<JobPosition> getJobPositionsForEmployee(Employee employee) {
        return employeeService.getJobPositionsForEmployee(employee);
    }

    public Employee login(String username, String password) {
        return employeeService.login(username, password);
    }
}
