package controller;

import model.Apply;
import model.JobPosition;
import service.ApplyService;
import service.GeneralService;
import utility.ObjectNotFoundException;

import java.util.List;

public class ApplyController implements Controller<Apply> {
    private ApplyService applyService = GeneralService.getInstance().getApplyService();

    public void insert(Apply apply) {
        applyService.insert(apply);
    }

    public void update(Apply apply) {
        applyService.update(apply);
    }

    public Apply searchById(Long id) throws ObjectNotFoundException {
        return applyService.searchById(id);
    }

    public List<Apply> findAll() {
        return applyService.findAll();
    }

    public List<JobPosition> getAppliedPositions(Long id) {
        return applyService.getAppliedPositions(id);
    }
}
