package view_console;

import controller.CompanyController;
import controller.GeneralController;
import model.ContractType;
import model.JobPosition;
import utility.Utilities;

import java.util.List;

public class CompanyView {
    private Register register = new Register();

    public void companyMenu() {
        System.out.println("Company :");
        System.out.println("1 - add position");
        System.out.println("2 - my positions");
        System.out.println("3 - update profile");
        System.out.println("enter a number : ");

        int number = Utilities.getInstance().getIntFromUser();

        switch (number) {
            case 1:
                addPosition();
                companyMenu();
                break;
            case 2:
                showMyPositions();
                companyMenu();
                break;
            case 3:
                register.getCompanyInformation(GetInfoType.UPDATE);
                companyMenu();
                break;
            case -1:
                Main.firstMenu();
                break;
            default:
                System.out.println("invalid number");
                companyMenu();
        }
    }

    private void showMyPositions() {
        List<JobPosition> jobPositions = GeneralController.getInstance().getJobPositionController()
                .searchByCompanyId(CompanyController.currentCompany.getId());

        register.showJobPositions(jobPositions);
    }

    private void addPosition() {
        System.out.println("Add job position : ");

        System.out.println("please enter your title : ");
        String title = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your description : ");
        String description = Utilities.getInstance().getStringFromUser();

        Long job_id = register.chooseJob();
        if (job_id == null)
            companyMenu();

        Long contract_type_id = chooseContractType();

        JobPosition jobPosition = new JobPosition(title, description, job_id, contract_type_id, CompanyController.currentCompany.getId());

        GeneralController.getInstance().getJobPositionController().insert(jobPosition);
        System.out.println("your position now is added");
        System.out.println();
    }

    private Long chooseContractType() {
        List<ContractType> contractTypes = GeneralController.getInstance().getContractTypeController().findAll();

        System.out.println("Jobs :");
        for (int i = 1; i <= contractTypes.size(); i++)
            System.out.println(i + " - type : " + contractTypes.get(i - 1).getType());

        System.out.println("please enter a contract type : ");
        int input = Utilities.getInstance().getIntFromUser();

        if (input == 0 || contractTypes.size() < input) {
            System.out.println("invalid input");
            addPosition();
        }

        return contractTypes.get(input - 1).getId();
    }

}
