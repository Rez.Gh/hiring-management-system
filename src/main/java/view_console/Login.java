package view_console;

import controller.CompanyController;
import controller.EmployeeController;
import controller.GeneralController;
import model.Company;
import model.Employee;
import utility.Utilities;

public class Login {
    private EmployeeView employeeView = new EmployeeView();
    private CompanyView companyView = new CompanyView();

    public void login() {
        System.out.println("Login :");
        System.out.println("1 - login as company");
        System.out.println("2 - login as employee");
        System.out.println("enter a number : ");

        int number = Utilities.getInstance().getIntFromUser();

        switch (number) {
            case 1:
                loginCompany();
                companyView.companyMenu();
                break;
            case 2:
                loginEmployee();
                employeeView.employeeMenu();
                break;
            case -1:
                Main.firstMenu();
                break;
            default:
                System.out.println("invalid number");
                login();
        }
    }

    private void loginEmployee() {
        System.out.println("Login as employee : ");
        System.out.println("please enter your username : ");
        String username = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your password :");
        String password = Utilities.getInstance().getStringFromUser();

        Employee employee = GeneralController.getInstance().getEmployeeController().login(username, password);

        if (employee == null)
            loginEmployee();

        System.out.println(employee.getName() + " now you are login");
        EmployeeController.currentEmployee = employee;
        System.out.println();
    }

    private void loginCompany() {
        System.out.println("Login as company : ");
        System.out.println("please enter your username : ");
        String username = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your password :");
        String password = Utilities.getInstance().getStringFromUser();

        Company company = GeneralController.getInstance().getCompanyController().login(username, password);

        if (company == null)
            loginCompany();

        System.out.println(company.getName() + " now you are login");
        CompanyController.currentCompany = company;
        System.out.println();
    }
}
