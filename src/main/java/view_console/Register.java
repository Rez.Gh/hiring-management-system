package view_console;

import controller.CompanyController;
import controller.GeneralController;
import model.Company;
import model.Employee;
import model.Job;
import model.JobPosition;
import utility.Utilities;

import java.util.List;

public class Register {

    public void register() {
        System.out.println("Register :");
        System.out.println("1 - register as company");
        System.out.println("2 - register as employee");
        System.out.println("enter a number : ");

        int number = Utilities.getInstance().getIntFromUser();

        switch (number) {
            case 1:
                getCompanyInformation(GetInfoType.REGISTER);
                break;
            case 2:
                getEmployeeInformation(GetInfoType.REGISTER);
                break;
            case -1:
                Main.firstMenu();
                break;
            default:
                System.out.println("invalid number");
                register();
        }
    }

    public void getEmployeeInformation(GetInfoType getInfoType) {
        if (getInfoType == GetInfoType.REGISTER)
            System.out.println("Register as employee : ");
        else
            System.out.println("Update your profile :");

        System.out.println("please enter your name : ");
        String name = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your username : ");
        String username = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your password : ");
        String password = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your age : ");
        int age = Utilities.getInstance().getIntFromUser();

        Long job_id = chooseJob();

        if(job_id == null)
            register();

        System.out.println("please enter your email : ");
        String email = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your telephone : ");
        String telephone = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your work experience : ");
        String work_experience = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter \"vip\" for vip membership : ");
        String vip = Utilities.getInstance().getStringFromUser();
        boolean is_vip = vip.toLowerCase().equals("vip");

        Employee employee = new Employee(name, username, password, age, job_id, email, telephone
                , work_experience, is_vip);

        if (getInfoType == GetInfoType.REGISTER) {
            GeneralController.getInstance().getEmployeeController().insert(employee);
            System.out.println("you are now registered");
        } else {
            GeneralController.getInstance().getEmployeeController().update(employee);
            System.out.println("your profile is now updated");
        }
        System.out.println();
    }

    public void getCompanyInformation(GetInfoType getInfoType) {
        if (getInfoType == GetInfoType.REGISTER)
            System.out.println("Register as company : ");
        else
            System.out.println("Update your company profile :");

        System.out.println("please enter your name : ");
        String name = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your username : ");
        String username = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your password : ");
        String password = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your location : ");
        String location = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your field : ");
        String field = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your description : ");
        String description = Utilities.getInstance().getStringFromUser();

        System.out.println("please enter your employee count : ");
        int employee_count = Utilities.getInstance().getIntFromUser();


        Company company = new Company(name, username, password, location, field, description, employee_count);

        if (getInfoType == GetInfoType.REGISTER) {
            GeneralController.getInstance().getCompanyController().insert(company);
            System.out.println("you are now registered");
        } else {
            company.setId(CompanyController.currentCompany.getId());
            GeneralController.getInstance().getCompanyController().update(company);
            System.out.println("your profile is now updated");
        }
        System.out.println();
    }

    Long chooseJob() {
        List<Job> jobs = GeneralController.getInstance().getJobController().findAll();

        if(jobs.size() == 0) {
            System.out.println("there is no job");
            return null;
        }

        System.out.println("Jobs :");
        for (int i = 1; i <= jobs.size(); i++) {
            System.out.println(i + " - title : " + jobs.get(i - 1).getTitle());
        }
        System.out.println("please enter a job : ");
        int input = Utilities.getInstance().getIntFromUser();

        if (input == 0 || jobs.size() < input) {
            System.out.println("invalid input");
            chooseJob();
        }

        return jobs.get(input - 1).getId();
    }

    void showJobPositions(List<JobPosition> jobPositions){
        JobPosition jobPosition;
        for (int i = 1; i <= jobPositions.size(); i++) {
            jobPosition = jobPositions.get(i - 1);
            System.out.println(i + " - title : " + jobPosition.getTitle() + ", view count : " + jobPosition.getView_count()
                    + ", description : " + jobPosition.getDescription());
        }
    }
}
