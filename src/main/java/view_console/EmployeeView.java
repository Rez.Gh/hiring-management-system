package view_console;

import controller.*;
import model.*;
import utility.Utilities;

import java.util.List;

public class EmployeeView {
    private Register register = new Register();

    public void employeeMenu() {
        System.out.println("Company :");
        System.out.println("1 - apply for a job position");
        System.out.println("2 - positions that you applied for");
        System.out.println("3 - update profile");
        System.out.println("enter a number : ");

        int number = Utilities.getInstance().getIntFromUser();

        switch (number) {
            case 1:
                applyingForPosition();
                employeeMenu();
                break;
            case 2:
                appliedPositions();
                employeeMenu();
                break;
            case 3:
                register.getEmployeeInformation(GetInfoType.UPDATE);
                employeeMenu();
                break;
            case -1:
                Main.firstMenu();
                break;
            default:
                System.out.println("invalid number");
                employeeMenu();
        }
    }

    private void applyingForPosition() {
        List<JobPosition> jobPositions = GeneralController.getInstance().getEmployeeController()
                .getJobPositionsForEmployee(EmployeeController.currentEmployee);

        if (jobPositions.size() == 0) {
            System.out.println("there is no job position for you");
            employeeMenu();
        }

        for (JobPosition jobPosition : jobPositions) {
            jobPosition.setView_count(jobPosition.getView_count() + 1);
            GeneralController.getInstance().getJobPositionController().updateByViewCount(jobPosition);
        }

        register.showJobPositions(jobPositions);

        System.out.println("please enter number of job you want to apply : ");

        int input = Utilities.getInstance().getIntFromUser();

        if (input == 0 || jobPositions.size() < input) {
            System.out.println("invalid input");
            employeeMenu();
        }

        Apply apply = new Apply(EmployeeController.currentEmployee.getId(),
                jobPositions.get(input - 1).getId(), String.valueOf(System.currentTimeMillis()));

        GeneralController.getInstance().getApplyController().insert(apply);

        System.out.println("you now apply for " + jobPositions.get(input - 1).getTitle() + " position");
    }

    private void appliedPositions() {
        List<JobPosition> jobPositions = GeneralController.getInstance().getApplyController()
                .getAppliedPositions(EmployeeController.currentEmployee.getId());

        if (jobPositions.size() == 0) {
            System.out.println("you dont apply for any job");
            employeeMenu();
        }

        register.showJobPositions(jobPositions);
    }
}
