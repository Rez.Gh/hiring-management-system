package model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Apply {
    private Long id;
    private Long employee_id;
    private Long job_position_id;
    private String date;

    public Apply(Long employee_id, Long job_position_id, String date) {
        this.employee_id = employee_id;
        this.job_position_id = job_position_id;
        this.date = date;
    }
}
