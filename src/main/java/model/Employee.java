package model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Employee {
    private Long id;
    private String name;
    private String username;
    private String password;
    private int age;
    private Long job_id;
    private String email;
    private String telephone;
    private String work_experience;
    private boolean is_vip;

    public Employee(String name, String username, String password, int age, Long job_id, String email,
                    String telephone, String work_experience, boolean is_vip) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.age = age;
        this.job_id = job_id;
        this.email = email;
        this.telephone = telephone;
        this.work_experience = work_experience;
        this.is_vip = is_vip;
    }
}
