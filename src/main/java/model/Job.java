package model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Job {
    private Long id;
    private String title;

    public Job(String title) {
        this.title = title;
    }
}
