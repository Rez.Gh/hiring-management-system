package model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ContractType {
    private Long id;
    private String type;

    public ContractType(String type) {
        this.type = type;
    }
}
