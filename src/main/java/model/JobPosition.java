package model;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class JobPosition {
    private Long id;
    private String title;
    private String description;
    private int view_count;
    private Long job_id;
    private Long contract_type_id;
    private Long company_id;

    public JobPosition(String title, String description, Long job_id, Long contract_type_id, Long company_id) {
        this.title = title;
        this.description = description;
        this.view_count = 0;
        this.job_id = job_id;
        this.contract_type_id = contract_type_id;
        this.company_id = company_id;
    }
}
