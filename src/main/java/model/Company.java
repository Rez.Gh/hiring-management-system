package model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Company {
    private Long id;
    private String name;
    private String username;
    private String password;
    private String location;
    private String field;
    private String description;
    private int employee_count;

    public Company(String name, String username, String password, String location, String field,
                   String description, int employee_count) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.location = location;
        this.field = field;
        this.description = description;
        this.employee_count = employee_count;
    }
}
