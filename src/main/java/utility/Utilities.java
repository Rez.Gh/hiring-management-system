package utility;

import java.util.*;

public class Utilities {

    private static Utilities utilities = null;
    private Scanner scanner;

    Utilities() {
        scanner = new Scanner(System.in);
    }

    public static Utilities getInstance() {
        if (utilities == null)
            utilities = new Utilities();
        return utilities;
    }

    public Integer getIntFromUser() {
        return Integer.parseInt(scanner.nextLine());
    }

    public String getStringFromUser() {
        return scanner.nextLine();
    }

    public Long getLongFromUser() {
        return Long.parseLong(scanner.nextLine());
    }

    public Date convertMillisecondToDate(Long millie) {
        Date date = new Date(millie);
        return date;
    }
}
