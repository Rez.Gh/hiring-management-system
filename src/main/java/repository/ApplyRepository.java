package repository;

import model.Apply;

import java.sql.SQLException;
import java.util.List;

public interface ApplyRepository extends Repository<Apply> {
    List<Apply> searchByEmployeeId(Long id) throws SQLException;
}
