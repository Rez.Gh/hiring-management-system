package repository;

import model.ContractType;
import repository.Repository;

public interface ContractTypeRepository extends Repository<ContractType> {

}
