package repository;

import model.Employee;
import utility.ObjectNotFoundException;

import java.sql.SQLException;

public interface EmployeeRepository extends Repository<Employee> {
    Employee searchByUsername(String username) throws SQLException, ObjectNotFoundException;
}
