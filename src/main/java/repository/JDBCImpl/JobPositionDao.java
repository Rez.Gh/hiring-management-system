package repository.JDBCImpl;

import connectionManager.*;
import model.JobPosition;
import repository.JobPositionRepository;
import utility.ObjectNotFoundException;

import java.sql.*;
import java.util.*;

public class JobPositionDao implements JobPositionRepository {

    public ConnectionManager connectionManager = new MySqlConnectionManager();
    private PreparedStatement statement;
    private static final String INSERT_QUERY = "insert into job_position (title, description, view_count, job_id," +
            "contract_type_id, company_id) values (?, ?, ?, ?, ?, ?)";
    private static final String SELECT_BY_ID_QUERY = "select * from job_position where job_position.id = ?";
    private static final String SELECT_ALL_QUERY = "select * from job_position";
    private static final String UPDATE_QUERY = "update job_position set title = ?, description = ?," +
            " view_count = ?, job_id = ?, contract_type_id = ?, company_id = ? where job_position.id = ?";
    private static final String DELETE_QUERY = "delete from job_position where job_position.id = ?";
    private static final String SELECT_BY_COMPANY_ID_QUERY = "select * from job_position where job_position.company_id = ?";
    private static final String SELECT_BY_JOB_ID_QUERY = "select * from job_position where job_id = ?";
    private static final String UPDATE_BY_VIEW_COUNT_QUERY = "update job_position set view_count = ? " +
            "where job_position.id = ?";

    public void insert(JobPosition jobPosition) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(INSERT_QUERY);
        statement.setString(1, jobPosition.getTitle());
        statement.setString(2, jobPosition.getDescription());
        statement.setInt(3, jobPosition.getView_count());
        statement.setLong(4, jobPosition.getJob_id());
        statement.setLong(5, jobPosition.getContract_type_id());
        statement.setLong(6, jobPosition.getCompany_id());
        statement.executeUpdate();
    }

    public JobPosition searchById(Long id) throws ObjectNotFoundException, SQLException {
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new JobPosition(resultSet.getLong(1), resultSet.getString(2),
                    resultSet.getString(3), resultSet.getInt(4),
                    resultSet.getLong(5), resultSet.getLong(6), resultSet.getLong(7));

        throw new ObjectNotFoundException("this job position does not exist");
    }

    public List<JobPosition> findAll() throws SQLException {
        List<JobPosition> jobPositions = new ArrayList<>();
        statement = connectionManager.getConnection().prepareStatement(SELECT_ALL_QUERY);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            jobPositions.add(new JobPosition(resultSet.getLong(1), resultSet.getString(2),
                    resultSet.getString(3), resultSet.getInt(4),
                    resultSet.getLong(5), resultSet.getLong(6), resultSet.getLong(7)));

        return jobPositions;
    }

    public void update(JobPosition jobPosition) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(UPDATE_QUERY);
        statement.setString(1, jobPosition.getTitle());
        statement.setString(2, jobPosition.getDescription());
        statement.setInt(3, jobPosition.getView_count());
        statement.setLong(4, jobPosition.getJob_id());
        statement.setLong(5, jobPosition.getContract_type_id());
        statement.setLong(6, jobPosition.getCompany_id());
        statement.setLong(7, jobPosition.getId());
        statement.executeUpdate();
    }

    public void delete(Long id) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(DELETE_QUERY);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    public List<JobPosition> searchByCompanyId(Long id) throws SQLException {
        List<JobPosition> jobPositions = new ArrayList<>();
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_COMPANY_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            jobPositions.add(new JobPosition(resultSet.getLong(1), resultSet.getString(2),
                    resultSet.getString(3), resultSet.getInt(4),
                    resultSet.getLong(5), resultSet.getLong(6), resultSet.getLong(7)));

        return jobPositions;
    }

    public void updateByViewCount(JobPosition jobPosition) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(UPDATE_BY_VIEW_COUNT_QUERY);
        statement.setInt(1, jobPosition.getView_count());
        statement.setLong(2, jobPosition.getId());
        statement.executeUpdate();
    }

    public List<JobPosition> searchByJobId(Long job_id) throws SQLException {
        List<JobPosition> jobPositions = new ArrayList<>();
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_JOB_ID_QUERY);
        statement.setLong(1, job_id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            jobPositions.add(new JobPosition(resultSet.getLong(1), resultSet.getString(2),
                    resultSet.getString(3), resultSet.getInt(4),
                    resultSet.getLong(5), resultSet.getLong(6), resultSet.getLong(7)));

        return jobPositions;
    }
}
