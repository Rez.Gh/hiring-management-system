package repository.JDBCImpl;

import connectionManager.*;
import model.ContractType;
import repository.ContractTypeRepository;
import utility.ObjectNotFoundException;

import java.sql.*;
import java.util.*;

public class ContractTypeDao implements ContractTypeRepository {
    public ConnectionManager connectionManager = new MySqlConnectionManager();
    private PreparedStatement statement;
    private static final String INSERT_QUERY = "insert into contract_type (type) values (?)";
    private static final String SELECT_BY_ID_QUERY = "select * from contract_type where contract_type.id = ?";
    private static final String SELECT_ALL_QUERY = "select * from contract_type";
    private static final String UPDATE_QUERY = "update contract_type set type = ? where contract_type.id = ?";
    private static final String DELETE_QUERY = "delete from contract_type where contract_type.id = ?";

    public void insert(ContractType contractType) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(INSERT_QUERY);
        statement.setString(1, contractType.getType());
        statement.executeUpdate();
    }

    public ContractType searchById(Long id) throws ObjectNotFoundException, SQLException {
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new ContractType(resultSet.getLong(1), resultSet.getString(2));

        throw new ObjectNotFoundException("this contract type does not exist");
    }

    public List<ContractType> findAll() throws SQLException {
        List<ContractType> contractTypes = new ArrayList<>();
        statement = connectionManager.getConnection().prepareStatement(SELECT_ALL_QUERY);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            contractTypes.add(new ContractType(resultSet.getLong(1), resultSet.getString(2)));

        return contractTypes;
    }

    public void update(ContractType contractType) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(UPDATE_QUERY);
        statement.setString(1, contractType.getType());
        statement.setLong(2, contractType.getId());
        statement.executeUpdate();
    }

    public void delete(Long id) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(DELETE_QUERY);
        statement.setLong(1, id);
        statement.executeUpdate();
    }
}
