package repository.JDBCImpl;

import connectionManager.*;
import model.Apply;
import repository.ApplyRepository;
import utility.ObjectNotFoundException;
import java.sql.*;
import java.util.*;

public class ApplyDao implements ApplyRepository {
    public ConnectionManager connectionManager = new MySqlConnectionManager();
    private PreparedStatement statement;
    private static final String INSERT_QUERY = "insert into apply (employee_id, job_position_id, apply_date) values (?, ?, ?)";
    private static final String SELECT_BY_ID_QUERY = "select * from apply where apply.id = ?";
    private static final String SELECT_ALL_QUERY = "select * from apply";
    private static final String UPDATE_QUERY = "update apply set employee_id = ?, job_position_id = ? ," +
            "apply_date = ? where apply.id = ?";
    private static final String DELETE_QUERY = "delete from apply where apply.id = ?";
    private static final String SELECT_BY_EMPLOYEE_ID_QUERY = "select * from apply where apply.employee_id = ?";

    public void insert(Apply apply) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(INSERT_QUERY);
        statement.setLong(1, apply.getEmployee_id());
        statement.setLong(2, apply.getJob_position_id());
        statement.setString(3, apply.getDate());
        statement.executeUpdate();
    }

    public Apply searchById(Long id) throws ObjectNotFoundException, SQLException {
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new Apply(resultSet.getLong(1), resultSet.getLong(2), resultSet.getLong(3),
                    resultSet.getString(4));

        throw new ObjectNotFoundException("this apply does not exist");
    }

    public List<Apply> findAll() throws SQLException {
        List<Apply> applies = new ArrayList<>();
        statement = connectionManager.getConnection().prepareStatement(SELECT_ALL_QUERY);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            applies.add(new Apply(resultSet.getLong(1), resultSet.getLong(2), resultSet.getLong(3),
                    resultSet.getString(4)));

        return applies;
    }

    public void update(Apply apply) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(UPDATE_QUERY);
        statement.setLong(1, apply.getEmployee_id());
        statement.setLong(2, apply.getJob_position_id());
        statement.setString(3, apply.getDate());
        statement.setLong(4, apply.getId());
        statement.executeUpdate();
    }

    public void delete(Long id) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(DELETE_QUERY);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    public List<Apply> searchByEmployeeId(Long id) throws SQLException {
        List<Apply> applies = new ArrayList<>();
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_EMPLOYEE_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            applies.add(new Apply(resultSet.getLong(1), resultSet.getLong(2), resultSet.getLong(3),
                    resultSet.getString(4)));

        return applies;
    }
}
