package repository.JDBCImpl;

import connectionManager.*;
import model.Employee;
import repository.EmployeeRepository;
import utility.ObjectNotFoundException;

import java.sql.*;
import java.util.*;

public class EmployeeDao implements EmployeeRepository {
    public ConnectionManager connectionManager = new MySqlConnectionManager();
    private PreparedStatement statement;
    private static final String INSERT_QUERY = "insert into employee (name, username, password, age, job_id, " +
            "email, telephone, work_experience, is_vip) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SELECT_BY_ID_QUERY = "select * from employee where employee.id = ?";
    private static final String SELECT_ALL_QUERY = "select * from employee";
    private static final String UPDATE_QUERY = "update employee set name = ?, username = ?, password = ?," +
            " age = ?, position = ?, email = ?, telephone = ?, work_experience = ?, is_vip = ? where employee.id = ?";
    private static final String DELETE_QUERY = "delete from employee where employee.id = ?";
    private static final String SELECT_BY_USERNAME_QUERY = "select * from employee where employee.username = ?";

    public void insert(Employee employee) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(INSERT_QUERY);
        statement.setString(1, employee.getName());
        statement.setString(2, employee.getUsername());
        statement.setString(3, employee.getPassword());
        statement.setInt(4, employee.getAge());
        statement.setLong(5, employee.getJob_id());
        statement.setString(6, employee.getEmail());
        statement.setString(7, employee.getTelephone());
        statement.setString(8, employee.getWork_experience());
        statement.setInt(9, (employee.is_vip() ? 1 : 0));
        statement.executeUpdate();
    }

    public Employee searchById(Long id) throws ObjectNotFoundException, SQLException {
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new Employee(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3),
                    resultSet.getString(4), resultSet.getInt(5), resultSet.getLong(6), resultSet.getString(7),
                    resultSet.getString(8), resultSet.getString(9), resultSet.getBoolean(10));

        throw new ObjectNotFoundException("this employee does not exist");
    }

    public List<Employee> findAll() throws SQLException {
        List<Employee> employees = new ArrayList<>();
        statement = connectionManager.getConnection().prepareStatement(SELECT_ALL_QUERY);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            employees.add(new Employee(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3),
                    resultSet.getString(4), resultSet.getInt(5), resultSet.getLong(6), resultSet.getString(7),
                    resultSet.getString(8), resultSet.getString(9), resultSet.getBoolean(10)));

        return employees;
    }

    public void update(Employee employee) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(UPDATE_QUERY);
        statement.setString(1, employee.getName());
        statement.setString(2, employee.getUsername());
        statement.setString(3, employee.getPassword());
        statement.setInt(4, employee.getAge());
        statement.setLong(5, employee.getJob_id());
        statement.setString(6, employee.getEmail());
        statement.setString(7, employee.getTelephone());
        statement.setString(8, employee.getWork_experience());
        statement.setInt(9, (employee.is_vip() ? 1 : 0));
        statement.setLong(10, employee.getId());
        statement.executeUpdate();
    }

    public void delete(Long id) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(DELETE_QUERY);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    public Employee searchByUsername(String username) throws SQLException, ObjectNotFoundException {
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_USERNAME_QUERY);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new Employee(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3),
                    resultSet.getString(4), resultSet.getInt(5), resultSet.getLong(6), resultSet.getString(7),
                    resultSet.getString(8), resultSet.getString(9), resultSet.getBoolean(10));

        throw new ObjectNotFoundException("this employee does not exist");
    }
}
