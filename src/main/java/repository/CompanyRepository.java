package repository;

import model.Company;
import utility.ObjectNotFoundException;

import java.sql.SQLException;

public interface CompanyRepository extends Repository<Company> {
    Company searchByUsername(String username) throws SQLException, ObjectNotFoundException;
}
