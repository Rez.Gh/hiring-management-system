package repository;

import model.JobPosition;

import java.sql.SQLException;
import java.util.List;

public interface JobPositionRepository extends Repository<JobPosition> {
    List<JobPosition> searchByCompanyId(Long id) throws SQLException;

    void updateByViewCount(JobPosition jobPosition) throws SQLException;
}
