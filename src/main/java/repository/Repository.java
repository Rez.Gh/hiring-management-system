package repository;

import java.sql.SQLException;
import java.util.List;

public interface Repository<E> {

    void insert(E e) throws SQLException;

    E searchById(Long id) throws Exception;

    List<E> findAll() throws SQLException;

    void update(E e) throws SQLException;

    void delete(Long id) throws SQLException;
}
