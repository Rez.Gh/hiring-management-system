package service;

import model.Apply;
import model.Employee;
import model.JobPosition;
import repository.GeneralRepository;
import repository.JDBCImpl.ApplyDao;
import utility.ObjectNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ApplyService implements Service<Apply> {
    private final int APPLY_LIMIT = 5;
    private ApplyDao applyDao = GeneralRepository.getInstance().getApplyDao();

    public void insert(Apply apply) {
        try {
            Employee employee = GeneralRepository.getInstance().getEmployeeDao().searchById(apply.getEmployee_id());
            if (!employee.is_vip())
                if (getAppliedPositions(employee.getId()).size() >= APPLY_LIMIT) {
                    System.out.println("your cant apply for more than " + APPLY_LIMIT +
                            " job position, please buy vip membership");
                    return;
                }

            applyDao.insert(apply);
        } catch (SQLException e) {
            System.out.println("can not insert this apply");
        } catch (ObjectNotFoundException e) {
            System.out.println("this employee does not exist");
        }
    }

    public void update(Apply apply) {
        try {
            applyDao.update(apply);
        } catch (SQLException e) {
            System.out.println("can not update this apply");
        }
    }

    public Apply searchById(Long id) throws ObjectNotFoundException {
        try {
            return applyDao.searchById(id);
        } catch (SQLException e) {
            throw new ObjectNotFoundException("this apply does not exist");
        }
    }

    public List<Apply> findAll() {
        try {
            return applyDao.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public List<JobPosition> getAppliedPositions(Long id) {
        try {
            List<JobPosition> jobPositions = new ArrayList<>();
            List<Apply> applies = applyDao.searchByEmployeeId(id);

            for (int i = 0; i < applies.size(); i++) {
                jobPositions.add(GeneralRepository.getInstance().getJobPositionDao()
                        .searchById(applies.get(i).getJob_position_id()));
            }
            return jobPositions;
        } catch (SQLException | ObjectNotFoundException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
