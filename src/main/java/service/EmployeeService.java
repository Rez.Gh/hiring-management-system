package service;

import model.Employee;
import model.JobPosition;
import repository.GeneralRepository;
import repository.JDBCImpl.EmployeeDao;
import utility.Encryptor;
import utility.ObjectNotFoundException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeService implements Service<Employee> {
    private EmployeeDao employeeDao = GeneralRepository.getInstance().getEmployeeDao();

    public void insert(Employee employee) {
        try {
            employee.setPassword(Encryptor.getMd5(employee.getPassword()));
            employeeDao.insert(employee);
        } catch (SQLException e) {
            System.out.println("can not insert this employee : " + employee.getName());
        }
    }

    public void update(Employee employee) {
        try {
            employeeDao.update(employee);
        } catch (SQLException e) {
            System.out.println("can not update this employee : " + employee.getName());
        }
    }

    public Employee searchById(Long id) throws ObjectNotFoundException {
        try {
            return employeeDao.searchById(id);
        } catch (SQLException e) {
            throw new ObjectNotFoundException("this employee does not exist");
        }
    }

    public List<Employee> findAll() {
        try {
            return employeeDao.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public Employee searchByUsername(String username) {
        try {
            return employeeDao.searchByUsername(username);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ObjectNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<JobPosition> getJobPositionsForEmployee(Employee employee) {
        try {
            List<JobPosition> jobPositions = GeneralRepository.getInstance().getJobPositionDao()
                    .searchByJobId(employee.getJob_id());
            List<JobPosition> alreadyApplied = GeneralService.getInstance().getApplyService()
                    .getAppliedPositions(employee.getId());

            for (JobPosition jobPosition : alreadyApplied) {
                for (int i = 0; i < jobPositions.size(); i++) {
                    if (jobPosition.getId().equals(jobPositions.get(i).getId()))
                        jobPositions.remove(jobPositions.get(i));
                }
            }
            return jobPositions;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public Employee login(String username, String password) {
        try {
            Employee employee = employeeDao.searchByUsername(username);
            if (employee.getPassword().equals(Encryptor.getMd5(password)))
                return employee;
            else
                System.out.println("wrong password");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ObjectNotFoundException e) {
            e.printStackTrace();
            System.out.println("this user does not exist");
        }
        return null;
    }
}
